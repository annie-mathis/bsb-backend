var express = require('express');
var router = express.Router();

var userRoutes = require('./subroutes/users.js');
var itemRoutes = require('./subroutes/items.js');
var orderRoutes = require('./subroutes/orders.js');
var staffRoutes = require('./subroutes/staff.js');

/* GET home page, located at /api/ */
router.get('/', function(req, res, next) {
  // res.send('Available Routes: users, items, orders, staff');
  res.json({message: "Hello there! shoop de boop"});
});

router.use('/users', userRoutes);
router.use('/items/', itemRoutes);
router.use('/orders/', orderRoutes);
router.use('/staff/', staffRoutes);

module.exports = router;
