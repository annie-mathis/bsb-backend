var express = require('express');
var router = express.Router();

/* STAFF PAGE -- /api/staff */
router.get('/', function(req, res, next) {
  // res.send('Available Routes: users, items, orders, staff');
  res.json({message: "STAFF PAGE"});
});

module.exports = router;
