var express = require('express');
var router = express.Router();

/* USERS PAGE -- /api/users */
router.get('/', function(req, res, next) {
  // res.send('Available Routes: users, items, orders, staff');
  res.json({message: "USERS PAGE"});
});

module.exports = router;
